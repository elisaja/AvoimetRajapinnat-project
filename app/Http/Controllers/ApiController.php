<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Restaurants;
use App\MenuItems;

class ApiController extends Controller{


    public function indexAction(){
        return view('interface');
    }

    public function showAll(){
        return view('restaurants');
    }

    public function showRestById($id){
        return view('restaurantMenu', ['restaurant_ID' => $id]);
    }

}