<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Restaurants;
use App\MenuItems;
use Illuminate\Support\Facades\DB;

class Front extends Controller
{
    //var $eventDates;
    var $restaurants;
    var $items;

    public function __construct()
    {
        //$this->eventDates = EventDate::all(array('Date')); // EventDate on malli-luokan nimi
        $this->restaurants = Restaurants::all(array('name'));
        $this->items = MenuItems::all(array('item_name', 'price', 'item_description'));
    }

    public function index()
    {
        return view('page', array('restaurants' => $this->restaurants, 'items' => $this->items));
    }
}