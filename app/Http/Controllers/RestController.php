<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Validator;
use App\Restaurants;
use App\MenuItems;

class RestController extends Controller
{

    /*
     * Näyttää kaikkien ravintoloiden tiedot. Kun parametrina on ravintolan ID,
     * näyttää kyseisen ravintolan tiedot ja menun.
     */
    public function restaurantList(Request $request) {
        $id = $this->sanitize($request->input('restaurant_ID'));
        //Kaikkien ravintoloiden tiedot
        if ($id == null) {
            $response = Restaurants::all();
        }
        //Yhden ravintolan menu ja tiedot
        else {
            $restaurant = Restaurants::where('restaurant_ID', $id)
                            ->get();
            $items = MenuItems::where('restaurant_ID', $id)
                            ->get();

            $response = array_merge($restaurant->toArray(), $items->toArray());
            if ($response == null){
                return Response::json(array('msg'=>'Restaurant not found.'));
            }
        }

        return Response::json($response);
}
/*
 * Lisää uuden ravintolan.
 */
    public function newRestaurant(Request $request) {
        $name = $this->sanitize($request->input('name'));
        $address = $this->sanitize($request->input('address'));
        $description = $this->sanitize($request->input('description'));


        $rules = array(
            'name'             => 'required',
            'address'            => 'required',
            'description'         => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            // get the error messages from the validator
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return redirect()->route('restAll')
                ->withErrors($validator)
                ->withInput($request->input());

        } else {

            $id = Restaurants::insertGetId(
                ['name' => $name,
                'address' => $address,
                'description' => $description]
            );

            return redirect()->route("restById", $id);
            //return Response::json(array('msg'=>'New restaurant added.'));
        }
    }

    /*
     * Muokkaa ravintolan tietoja.
     */
    public function editRestaurant(Request $request){
        $restaurant_ID = $this->sanitize($request->input('rest_ID'));
        $name = $this->sanitize($request->input('name'));
        $address = $this->sanitize($request->input('address'));
        $description = $this->sanitize($request->input('description'));



        //Löytyykö tietokannasta ravintola-ID
        $findID = Restaurants::where('restaurant_ID', '=', $restaurant_ID)
            ->first();

        //Jos löytyy
        if ($findID != null) {
            if ($name != null) {
                Restaurants::where('restaurant_ID', '=', $restaurant_ID)
                    ->update(['name' => $name]);
            }
            if ($address != null) {
                Restaurants::where('restaurant_ID', '=', $restaurant_ID)
                    ->update(['address' => $address]);
            }
            if ($description != null) {
                Restaurants::where('restaurant_ID', '=', $restaurant_ID)
                    ->update(['description' => $description]);
            }

            return redirect()->route("restById", $restaurant_ID);
        }
        //Jos ei löydy
        else {
            return Response::json(array('msg'=>'Restaurant not found.'));
        }
    }

    /*
     * Poistaa ravintolan ja sen ruokalistan.
     */
    public function deleteRestaurant(Request $request){
        $restaurant_ID = $this->sanitize($request->input('rest_ID'));

        //Löytyykö tietokannasta ravintola-ID
        $findID = Restaurants::where('restaurant_ID', '=', $restaurant_ID)
            ->first();

        //Jos löytyy
        if ($findID != null) {
            MenuItems::where('restaurant_ID', '=', $restaurant_ID)
                ->delete();

            Restaurants::where('restaurant_ID', '=', $restaurant_ID)
                ->delete();

            return redirect()->route("restAll");
        }
        //Jos ei löydy
        else {
            return Response::json(array('msg'=>'Restaurant not found.'));
        }
    }

    /*
     * Lisää tietyn ravintolan menuun ruoan.
     */
    public function newMenuItem(Request $request) {
        $restaurant_ID = $this->sanitize($request->input('rest_ID'));
        $item_name = $this->sanitize($request->input('name'));
        $price = $this->sanitize($request->input('price'));
        $item_description = $this->sanitize($request->input('desc'));
        $extra_info = $this->sanitize($request->input('info'));

        $rules = array(
            'rest_ID'             => 'required',
            'name'            => 'required',
            'price'         => 'required',
            'desc'            => 'nullable',
            'info'         => 'nullable'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            // get the error messages from the validator
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return redirect()->route('restById', $restaurant_ID)
                ->withErrors($validator)
                ->withInput($request->input());

        } else {

            //Löytyykö tietokannasta ravintola-ID
            $findResID = Restaurants::where('restaurant_ID', '=', $restaurant_ID)
                ->first();

            //Jos löytyy
            if ($findResID != null) {
                MenuItems::insertGetId(
                    ['restaurant_ID' => $restaurant_ID,
                        'item_name' => $item_name,
                        'price' => $price,
                        'item_description' => $item_description,
                        'extra_info' => $extra_info]
                );
                return redirect()->route("restById", $restaurant_ID);
            }
            //Jos ei löydy
            else {
                return Response::json(array('msg'=>'Restaurant not found.'));
            }
        }
    }

    /*
     * Muokkaa ruoan tietoja.
     */
    public function editItem(Request $request){
        $item_ID = $this->sanitize($request->input('item_ID'));
        $name = $this->sanitize($request->input('name'));
        $price = $this->sanitize($request->input('price'));
        $description = $this->sanitize($request->input('description'));
        $info = $this->sanitize($request->input('info'));
        $restId=MenuItems::where("item_ID", '=', $item_ID)
        ->value('restaurant_ID');

        //Löytyykö tietokannasta item-ID
        $findID = MenuItems::where('item_ID', '=', $item_ID)
            ->first();

        //Jos löytyy
        if ($findID != null) {
            if ($name != null) {
                MenuItems::where('item_ID', '=', $item_ID)
                    ->update(['item_name' => $name]);
            }
            if ($price != null) {
                MenuItems::where('item_ID', '=', $item_ID)
                    ->update(['price' => $price]);
            }
            if ($description != null) {
                MenuItems::where('item_ID', '=', $item_ID)
                    ->update(['item_description' => $description]);
            }
            if ($info != null) {
                MenuItems::where('item_ID', '=', $item_ID)
                    ->update(['extra_info' => $info]);
            }
            return redirect()->route("restById", $restId);
        }
        //Jos ei löydy
        else {
            return Response::json(array('msg'=>'Menu item not found.'));
        }
    }

    /*
     * Poistaa ruoan ravintolan menusta.
     */
    public function deleteMenuItem(Request $request){
        $item_ID = $this->sanitize($request->input('item_ID'));

        //Löytyykö tietokannasta item-ID
        $findItemID = MenuItems::where('item_ID', '=', $item_ID)
            ->first();

        //Jos löytyy
        if ($findItemID != null) {
            MenuItems::where('item_ID', '=', $item_ID)
                ->delete();
            return Response::json(array('msg' => 'Menu item deleted.'));
        }
        //Jos ei löydy
        else {
            return Response::json(array('msg'=>'Menu item not found.'));
        }
    }

    /*
     * Syötteiden puhdistus.
     */

    public function sanitize($string) {
        return $sanitized = filter_var($string, FILTER_SANITIZE_STRING);
    }


}
