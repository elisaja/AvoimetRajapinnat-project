<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $item_ID
 * @property integer $restaurant_ID
 * @property string $item_name
 * @property float $price
 * @property string $description
 * @property string $extra_info
 * @property Restaurant $restaurant
 */
class MenuItems extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['restaurant_ID', 'item_name', 'price', 'item_description', 'extra_info'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'menu_items';

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant', 'restaurant_ID', 'restaurant_ID');
    }
}
