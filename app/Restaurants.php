<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $restaurant_ID
 * @property string $name
 * @property string $address
 * @property string $description
 * @property MenuItem[] $menuItems
 */
class Restaurants extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'address', 'description'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurants';

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menuItems()
    {
        return $this->hasMany('App\MenuItem', 'restaurant_ID', 'restaurant_ID');
    }
}
