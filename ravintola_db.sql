--
-- Database: `ravintola_db`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `menu_items`
--

CREATE TABLE `menu_items` (
  `item_ID` int(11) NOT NULL,
  `restaurant_ID` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `extra_info` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `menu_items`
--

INSERT INTO `menu_items` (`item_ID`, `restaurant_ID`, `item_name`, `price`, `item_description`, `extra_info`) VALUES
(1, 1, 'Pizza Margherita', '8.79', 'Tomato sauce, cheese', NULL),
(2, 2, 'Peking Duck', '12.99', 'Beijing Roast Duck in Sweet and Sour Marinade', NULL),
(3, 1, 'Pizza Viennese', '11.49', 'Tomato, mozzarella, German sausage, oregano, oil', 'Gluten-free'),
(4, 3, 'Apple Pie', '4.99', 'American style apple pie with vanilla sauce', 'Contains apples'),
(5, 2, 'Fried Chicken', '9.99', 'Crunchy and delicious', NULL),
(6, 1, 'Pizza Alfredo', '11.99', 'Chicken, Alfredo sauce, garlic', NULL),
(8, 5, 'Pizza Hawaii', '7.99', 'Ham, pineapple', NULL),
(11, 1, 'Gelato', '3.99', 'Italian home-made ice cream', 'Contains ice cream');

-- --------------------------------------------------------

--
-- Rakenne taululle `restaurants`
--

CREATE TABLE `restaurants` (
  `restaurant_ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `restaurants`
--

INSERT INTO `restaurants` (`restaurant_ID`, `name`, `address`, `description`) VALUES
(1, 'Papa''s Pizzeria', 'Bulevardi 23', 'The best pizza you''ve ever had!'),
(2, 'Oriental House', 'Mannerheimintie 1', 'Asian Cuisine'),
(3, 'New York Steakhouse', 'Paatsamatie', 'All American diner'),
(5, 'Kotipizza', 'Laukkarinne', 'Sort of like pizza maybe');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`item_ID`),
  ADD KEY `restaurant_ID` (`restaurant_ID`);

--
-- Indexes for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`restaurant_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `item_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `restaurant_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Rajoitteet vedostauluille
--

--
-- Rajoitteet taululle `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_ibfk_1` FOREIGN KEY (`restaurant_ID`) REFERENCES `restaurants` (`restaurant_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
