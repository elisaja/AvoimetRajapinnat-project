# RESTaurants

*Maria Osuala ja Elisa Jalava*

RESTaurants on websovellus, joka tallentaa ravintoloiden tietoja ja niiden ruokalistoja.

## Projektissa käytetty

- Laravel
- XAMPP
- SQL

## Tietokanta

Tietokanta löytyy tiedostosta *ravintola_db.sql* projektin juurikansiosta.

Tietokannassa on kaksi taulua, *restaurants* (pääavain restaurant_ID) ja *menu_items* (pääavain item_ID ja viiteavain restaurant_ID).

## Käyttöliittymä

*/restaurants/all*

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Etusivu, jolla kaikki ravintolat.

*/restaurants/1*
	
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Näyttää ravintolan, jonka id on 1, menun ja tiedot.

Käyttöliittymä toimii kolmella bladellä, jotka sisältävät HTML:ää ja JavaScriptiä. *interface.blade.php* on master blade, jota *restaurants.blade* ja *restaurantMenu.blade* extendaavat.
Näkymiä kutsutaan ApiControllerissa.

## REST-kutsut

*/api/restaurants*
    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kaikkien ravintoloiden tiedot JSON-muodossa.

*/api/restaurants?restaurant_ID=1*

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ravintolan, jonka id=1 tiedot ja menu JSON-muodossa.

*/api/addRestaurant?name=Nimi&address=Osoite&description=Ravintolan kuvaus*
    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lisää ravintolan tietokantaan.

*/api/editRestaurant?rest_ID=1&name=Nimi&address=Osoite&description=Ravintolan kuvaus*
    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Muokkaa ravintolaa. ID pakollinen, muut valinnaisia. (Voi esim muokata vain nimeä).

*/api/deleteRestaurant?rest_ID=1*

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Poistaa ravintolan ja sen ruokalistan.

*/api/addItem?rest_ID=1&name=Nimi&price=9.9&desc=Kuvaus&info=Extra info*

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lisää ruoan ravintolan, jonka id=1, menuun. Kuvaus ja info valinnaisia.

*/api/editItem?item_ID=4&name=Nimi&price=4.99&description=Kuvaus&info=info*

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Muokkaa ruokaa. ID pakollinen, muut valinnaisia.

*/api/deleteItem?item_ID=9*

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Poistaa ruoan ravintolan menusta.


RestController hoitaa REST apin toiminnan. RestController vastaanottaa pyynnöt, jotka se tallentaa tietokantaan tai hakee tietokannasta. Validator tarkistaa, että lomakkeissa on kaikki tarvittavat tiedot ja palauttaa virheet, jos sellaisia on. Virheet näytetään käyttöliittymän lomakkeessa.

PHP:n FILTER_SANITIZE_STRING-filtteri puhdistaa syötteet ja estää esimerkiksi html-tagien kirjoittamisen.

## JSON-palautukset

Esimerkiksi yhden ravintolan tiedot ja menu palautuvat seuraavanlaisena JSON-tekstinä.

```json
[  
   {  
      "restaurant_ID":1,
      "name":"Papa's Pizzeria",
      "address":"Bulevardi 23",
      "description":"The best pizza you've ever had!"
   },
   {  
      "item_ID":1,
      "restaurant_ID":1,
      "item_name":"Pizza Margherita",
      "price":"8.79",
      "item_description":"Tomato sauce, cheese",
      "extra_info":null
   }
]
```