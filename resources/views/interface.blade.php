<!DOCTYPE html>
<html>
<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">

</head>
<body>
<nav class="navbar navbar-default">
<div class="container-fluid" id="navipalkki">
    <form method="post">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('restAll') }}">RESTaurants</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <span class="glyphicon glyphicon-cutlery" aria-hidden="true"></span>
                    Ravintolat
                    <span class="caret"></span></a>
                <ul class="dropdown-menu" id="restaurants">
                </ul>
            </li>
            <li role="presentation"><a onclick="showAddForm()">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    Lisää uusi ravintola</a></li>
        </ul>
    </form>
</div>
</nav>

<div class="container-fluid" id="addForm">
    <form method="GET" action="{{ route("addRestaurant") }}" novalidate>
    <div class="form-group @if ($errors->has('name')) has-error @endif">
        <label for="restName">Restaurant Name</label>
        <input type="text" id="restName" class="form-control" name="name" value="{{ old('name') }}" autocomplete="off">
        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
    </div>

    <div class="form-group @if ($errors->has('address')) has-error @endif">
        <label for="restAddress">Restaurant Address</label>
        <input type="text" id="restAddress" class="form-control" name="address" value="{{ old('address') }}" autocomplete="off">
        @if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
    </div>

    <div class="form-group @if ($errors->has('description')) has-error @endif">
        <label for="restDescription">Description</label>
        <input type="text" id="restDescription" class="form-control" name="description" value="{{ old('description') }}" autocomplete="off">
        @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
    </div>

    <button type="submit" class="btn btn-primary">Add Restaurant!</button>
    </form>

</div>

<!--TÄHÄN TULEE SIVUN SISÄLTÖ-->
<div class="container">
    @yield('content')
</div>

<script type="text/javascript">

    //Avaa ravintolanlisäysikkunan jos käyttäjä on saanut virheviestin siihen
    @if (count($errors) > 0)
        showAddForm();
    @endif

    var url = "{{ route('restaurantList') }}";
    listRestaurants(url);

    function listRestaurants(url) {

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {

            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var myArr = JSON.parse(xmlhttp.responseText);
                Call(myArr);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    function Call(arr) {
        var out = "";

        for (var i = 0; i < arr.length; i++) {

            var restUrl = '{{ route("restById", ":id") }}';
            restUrl = restUrl.replace(':id', arr[i].restaurant_ID);

            out += "<li><a href='" + restUrl + "' onclick=showMenu(" + arr + ")>" +
                arr[i].name + "</a></li>";
            console.log(arr[i].name);

        }
        document.getElementById("restaurants").innerHTML = out;

    }

    function showAddForm(){
        document.getElementById("addForm").style="visibility:visible";
    }

</script>


</body>
</html>