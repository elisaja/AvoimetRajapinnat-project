@extends('master')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <p>This is my body content.</p>
    @foreach ($restaurants as $restaurant)

        {{$restaurant}} <br>
    @endforeach
    @foreach ($items as $item)

        {{$item}} <br>
    @endforeach
@endsection