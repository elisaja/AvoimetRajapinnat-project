@extends('interface')
@section('content')

    <div id="restaurantInfo">

    </div>
    <!-- add menu item form-->
    <div class="container-fluid" id="addMenuForm"  style="display:none">
        <form class="form-group" id="addMenuItem" method="get" action="{{ route("addItem") }}">
            <input id="restaurantID" type="hidden" name="rest_ID" value="<?php echo $restaurant_ID?>">

            <div class="form-group @if ($errors->has('name')) has-error @endif">
                <label for="itemName">Item Name*</label>
                <input type="text" id="itemName" class="form-control" name="name" autocomplete="off">
                @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
            </div>
            <div class="form-group @if ($errors->has('price')) has-error @endif">
                <label for="price">Item Price*</label>
                <input type="number" step=any min="0" id="price" class="form-control" name="price" autocomplete="off">
                @if ($errors->has('price')) <p class="help-block">{{ $errors->first('price') }}</p> @endif
            </div>
            <div class="form-group @if ($errors->has('desc')) has-error @endif">
                <label for="itemDesc">Item Description</label>
                <input type="text" id="itemDesc" class="form-control" name="desc" autocomplete="off">
                @if ($errors->has('desc')) <p class="help-block">{{ $errors->first('desc') }}</p> @endif
            </div>
            <div class="form-group @if ($errors->has('info')) has-error @endif">
                <label for="extraInfo">Extra info</label>
                <input type="text" id="extraInfo" class="form-control" name="info" value="" autocomplete="off">
                @if ($errors->has('info')) <p class="help-block">{{ $errors->first('info') }}</p> @endif
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
    </div>
    <button class="btn btn-default" onclick="showEditForm()">Edit Restaurant</button>

    <!-- Edit Restaurant Form -->
    <div id="editFormDisplay" style="display:none">
        <form class="form-group" id="editRestaurant" method="get" action="{{ route("editRestaurant") }}">
            <input id="restaurantID" type="hidden" name="rest_ID" value="<?php echo $restaurant_ID?>">
            <div class="form-group @if ($errors->has('name')) has-error @endif">
                <label for="restName">Restaurant Name</label>
                <input type="text" id="restName" class="form-control" name="name" value="{{ old('name') }}" autocomplete="off">
                @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
            </div>

            <div class="form-group @if ($errors->has('address')) has-error @endif">
                <label for="restAddress">Restaurant Address</label>
                <input type="text" id="restAddress" class="form-control" name="address" value="{{ old('address') }}" autocomplete="off">
                @if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
            </div>

            <div class="form-group @if ($errors->has('description')) has-error @endif">
                <label for="restDescription">Description</label>
                <input type="text" id="restDescription" class="form-control" name="description" value="{{ old('description') }}" autocomplete="off">
                @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
            </div>
            <button type="submit" class="btn btn-primary">Edit Restaurant</button>
        </form>
    </div>
    <!--Delete Restaurant Button-->
    <div>
        <form class="form-group" id="deleteRestaurant" method="get" action="{{ route("deleteRestaurant") }}">
            <input id="restID" type="hidden" name="rest_ID" value="<?php echo $restaurant_ID?>">
            <button type="submit" class="btn btn-default" id="submitDelete">Delete Restaurant</button>
        </form>
    </div>

    <!--Edit menu item form -->
    <div class="container-fluid" id="editMenuForm"  style="display:none">
        <form class="form-group" id="editMenuItem" method="get" action="{{ route("editItem") }}">
            <input id="editItemID" type="hidden" name="item_ID">

            <div class="form-group @if ($errors->has('name')) has-error @endif">
                <label for="itemEditName">Item Name*</label>
                <input type="text" id="itemEditName" class="form-control" name="name" autocomplete="off">
                @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
            </div>
            <div class="form-group @if ($errors->has('price')) has-error @endif">
                <label for="editprice">Item Price*</label>
                <input type="number" step=any min="0" id="editprice" class="form-control" name="price" autocomplete="off">
                @if ($errors->has('price')) <p class="help-block">{{ $errors->first('price') }}</p> @endif
            </div>
            <div class="form-group @if ($errors->has('description')) has-error @endif">
                <label for="itemEditDesc">Item Description</label>
                <input type="text" id="itemEditDesc" class="form-control" name="description" autocomplete="off">
                @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
            </div>
            <div class="form-group @if ($errors->has('info')) has-error @endif">
                <label for="extraEditInfo">Extra info</label>
                <input type="text" id="extraEditInfo" class="form-control" name="info" value="" autocomplete="off">
                @if ($errors->has('info')) <p class="help-block">{{ $errors->first('info') }}</p> @endif
            </div>
            <button type="submit" class="btn btn-primary">Edit Item</button>
        </form>
    </div>

    <script type="text/javascript">

        //Avaa iteminlisäysikkunan jos käyttäjä on saanut virheviestin siihen
        @if (count($errors) > 0)
            addMenu();
                @endif

        var url = "{{ route('restaurantList', 'restaurant_ID='.$restaurant_ID) }}";
        getRest(url);
        console.log(url);
        function showEditForm(){
            document.getElementById("editFormDisplay").style="display:block";
        }

        $("#submitDelete").click(function(event){
            if(!confirm("Are you sure you want to delete this restaurant?"))
                event.preventDefault();
        });

        var myForm = document.getElementById('addMenuItem');
        myForm.addEventListener('submit', function () {
            var allInputs = myForm.getElementsByTagName('input');

            for (var i = 0; i < allInputs.length; i++) {
                var input = allInputs[i];

                if (input.name && !input.value) {
                    input.name = '';
                }
            }
        });
        var myForm2 = document.getElementById('editMenuItem');
        myForm2.addEventListener('submit', function () {
            var allInputs = myForm2.getElementsByTagName('input');

            for (var i = 0; i < allInputs.length; i++) {
                var input = allInputs[i];

                if (input.name && !input.value) {
                    input.name = '';
                }
            }
        });

        function getRest(url){

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {

                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var myArr = JSON.parse(xmlhttp.responseText);
                    showMenu(myArr);
                    console.log(myArr);
                }
            };
            xmlhttp.open("GET", url, true);
            xmlhttp.send();
        }

        function showMenu(arr){

            console.log(arr);
            //Ravintolan tiedot
            var out="<div class=''>" +
                        "<p class='lead'>" + arr[0].name + "</br>" +
                            "<small class='text-muted'>" + arr[0].description + "</br>" +
                                "<small>" + arr[0].address + "</small>" +
                            "</small>" +
                        "</p>" +

                    "</div>" +
                    "<div  class='panel panel-warning'>" +
                        "<div class='panel-heading'><b>Menu</b>" +
                        "</div>";
            console.log(arr[0].address);

            //Menu
            out+="<table class='table table'>";
            if (arr[1] == null) {
                out += "<tr><td>Looks like there's nothing on the menu.</td></tr>";
            }
            for (var i = 1; i<arr.length; i++){

                //Ottaa pois "null" arvot valinnaisista kentistä
                var desc = arr[i].item_description;
                if (desc == null) {
                    desc = "";
                }
                var info = arr[i].extra_info;
                if (info == null) {
                    info = "";
                }

                out+="<tr>"+
                    "<td>"+i+"</td>" +
                    "<td>"+arr[i].item_name+"</td>" +
                    "<td>"+desc+"</td>" +
                    "<td>"+arr[i].price+"</td>" +
                    "<td>"+info+"</td>" +
                    "<td><input type='button' class='btn btn-success' onclick='editMenuItem("+arr[i].item_ID+")' value='Edit'>" +
                    "<td><input type='button' class='btn btn-danger' onclick='removeMenuItem("+arr[i].item_ID+")' value='Remove'></tr>";

                console.log(arr[i].address);

            }

            out+="<tr><td colspan=7><input type='button' class='btn btn-default center-block' onclick='addMenu()' value='Add menu item'></td></tr></table>";
            document.getElementById("restaurantInfo").innerHTML=out;
            //document.getElementById("restaurantInfo").style='visibility:visible';
        }

        function removeMenuItem(id){

                var xhttp = new XMLHttpRequest();
                var url = '{{ route('deleteItem', 'item_ID='.':id') }}';
                url = url.replace(':id', id);
                xhttp.onreadystatechange = function() {
                    if (xhttp.readyState == 4 && xhttp.status == 200) {
                        var respText=JSON.parse(xhttp.responseText);
                        console.log(respText);
                    }
                }
                xhttp.open("GET", url, false);
                xhttp.send();
                window.location.reload();


        }
        function editMenuItem(id){
            document.getElementById("editItemID").value = id;
            document.getElementById("editMenuForm").style="display:block";
        }
        function addMenu(){
            document.getElementById("addMenuForm").style="display:block";
        }

    </script>


@endsection