@extends('interface')
@section('content')

    <div id="restList">

    </div>

    <script type="text/javascript">

        var url = "{{ route('restaurantList') }}";
        listRest(url);

        function listRest(url){

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {

                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var myArr = JSON.parse(xmlhttp.responseText);
                    showRest(myArr);
                }
            };
            xmlhttp.open("GET", url, true);
            xmlhttp.send();
        }

        function showRest(arr){
            var out = "<div  class='panel panel-warning'>" +

                "<div class='panel-heading allRest'>All Restaurants</div><div class='list-group'>";

            //Aakkosjärjestys
            arr.sort( function( a, b ) {
                a = a.name.toLowerCase();
                b = b.name.toLowerCase();

                return a < b ? -1 : a > b ? 1 : 0;
            });

            for (var i = 0; i<arr.length; i++){

                // Routen parametriksi restaurant-ID
                var restUrl = '{{ route("restById", ":id") }}';
                restUrl = restUrl.replace(':id', arr[i].restaurant_ID);
                console.log(restUrl);

                out += "<a class='list-group-item list-group-item-action' href='" + restUrl + "'>"+
                            "<div class='restListName'>" + arr[i].name +
                                "<br><small class='text-right'>"+arr[i].description+"</small>" +
                                "<small class='pull-right'>"+arr[i].address+"</small>" +
                            "</div>" +
                       "</a>";
                    console.log(arr[i].address);

            }
            out += "</div>";
            document.getElementById("restList").innerHTML=out;

        }

    </script>

@endsection