<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', 'Front@index');


// REST
Route::group(array('prefix' => 'api'), function() {

    Route::get('/restaurants', 'RestController@restaurantList')->name('restaurantList');

    Route::get('/addRestaurant', 'RestController@newRestaurant')->name('addRestaurant');
    Route::get('/editRestaurant', 'RestController@editRestaurant')->name('editRestaurant');
    Route::get('/deleteRestaurant', 'RestController@deleteRestaurant')->name('deleteRestaurant');

    Route::get('/addItem', 'RestController@newMenuItem')->name('addItem');
    Route::get('/editItem', 'RestController@editItem')->name('editItem');
    Route::get('/deleteItem', 'RestController@deleteMenuItem')->name('deleteItem');

});

// UI
Route::get('/restaview', 'ApiController@indexAction');

Route::group(array('prefix' => 'restaurants'), function() {
    Route::get('/all', 'ApiController@showAll')->name('restAll');
    Route::get('/{id}', 'ApiController@showRestById')->name('restById');
});

