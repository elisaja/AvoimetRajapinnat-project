<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class test extends TestCase
{
    /**
     * Testing database connection.
     *
     * @return void
     */
    public function testDatabase()
    {
        // Make call to application...

        $this->seeInDatabase('restaurants', ['name' => 'test']);
    }
}
